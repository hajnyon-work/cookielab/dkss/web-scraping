// For more information, see https://crawlee.dev/
import { CheerioCrawler, ProxyConfiguration } from 'crawlee';
import { router } from './routes.js';

const startUrls = ['https://www.cookielab.io'];

const crawler = new CheerioCrawler({
    requestHandler: router,
});

await crawler.run(startUrls);
