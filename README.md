# Crawlee example

See branches:
- `site-crawling` for simple crawler example
- `site-scraping` for simple scraper example

## Usage

```
npm install
npm run start
```